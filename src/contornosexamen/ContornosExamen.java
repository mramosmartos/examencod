/*
*Realiza un programa que inserte en un TreeMap, cinco productos (1 punto).

*Los productos son objetos, que tienen nombre y precio, además de los métodos set y get correspondientes para estos atributos. (-2 puntos si no se hace)

*La clave del TreeMap es el número de serie. (-1 punto si no se hace)

*Luego de insertarlos, liste por pantalla los cinco productos (2 puntos).

*Envía el código correspondiente comentado para generar javadoc (1 punto).
*/
package contornosexamen;

import java.text.DateFormat;
import java.util.*;
import javax.swing.JOptionPane;

/**
 *
     * @author Marina Ramos Martos
 */
public class ContornosExamen {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        Map <String,Productos> producto = new TreeMap();     
        //instanciamos la clase productos
        Productos productos= new Productos();
        productos.guardarelementosYVer(producto);

    }
}
